<div style="margin:auto;align:center"><img src="https://gitlab.com/algoritmos-iii/strong-games/strongames-api/wikis/uploads/db11b6e55ec630eabbf2fa9145f8ea43/Logo-StronGames-Min-Qualidade-Small.png" alt="Logo StronGames" title="Logo StronGames" height="59" width="240"></div>

# StronGames-API


## Descrição do Projeto

O projeto StronGames simula uma biblioteca virtual de jogos eletronicos. 
Nela voce pode cadastrar a lista de seus jogos favoritos com informações diversas e compartilhar suas experiencias vividas nos mundos virtuais com seus amigos.

## Pré-Requisitos

O projeto StronGames utiliza em sua api o framework Spring Boot para o desenvolvimento. Dito isso para rodar o projeto você precisa ter instalado na máquina:

*  Uma IDE (IntelliJ é o mais recomendado)
*  MySQL
*  Apache
*  Java e JVM
*  No caso do linux via terminal você pode rodar usando o comando **./mvnw spring-boot:run**

## Tecnologias Utilizadas

*  Spring Boot 2.x.x
*  MySQL
*  Apache
*  Java

## Como rodar 

Após fazer o clone do projeto abra o mesmo na IDE, aguarde atualizar as dependências e então rode o projeto. Ative o Apache e o MySQL. 
Teste as requisições utilizando o sofware Postman.
No linux via terminal e possível rodar o projeto usando o comando **./mvnw spring-boot:run**.


## Lincença Utilizada

*  Mit Licence
*  Link para licença: https://gitlab.com/algoritmos-iii/strong-games/strongames-api/blob/master/LICENSE